const got = require("./test/gotData");

function peopleNameOfAllHouses() {
  try {
    if (typeof got === "object") {
      houses = got.houses;
      return houses.reduce((housedata, house) => {
        housedata[house["name"]] = house["people"].reduce(
          (peopleData, people) => {
            peopleData.push(people["name"]);
            return peopleData;
          },
          []
        );
        return housedata;
      }, {});
    } else {
      throw Error("given data is not an object");
    }
  } catch (error) {
    console.log(error.name, error.message);
  }
}
module.exports = peopleNameOfAllHouses;
