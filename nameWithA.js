//Write a function called `nameWithA` which returns a array of names of all the people in `got` variable whose name includes `a` or `A`.

const got = require("./test/gotData");

function nameWithA() {
  try {
    if (typeof got === "object") {
      houses = got.houses;
      return houses
        .reduce((houseData, house) => {
          houseData.push(
            house.people.reduce((peopleData, people) => {
              if (
                people["name"].includes("A") ||
                people["name"].includes("a")
              ) {
                peopleData.push(people["name"]);
              }
              return peopleData;
            }, [])
          );
          return houseData;
        }, [])
        .flat();
    } else {
      throw Error("given data is not an object");
    }
  } catch (error) {
    console.error(error.name, error.message);
  }
}

module.exports = nameWithA;
