// Write a function called `nameWithS` which returns a array of names of all the people in `got` variable whose name includes `s` or `S`.

const got = require("./test/gotData");

function nameWithS() {
  try {
    if (typeof got === "object") {
      houses = got.houses;
      return houses
        .reduce((namesWithS, house) => {
          housePeople = house["people"];
          namesWithS.push(
            housePeople.reduce((sNames, people) => {
              if (
                people["name"].includes("S") ||
                people["name"].includes("s")
              ) {
                sNames.push(people["name"]);
              }
              return sNames;
            }, [])
          );
          return namesWithS;
        }, [])
        .flat();
    } else {
      throw Error("given data is not an object");
    }
  } catch (error) {
    console.error(error.name, error.message);
  }
}

module.exports = nameWithS;
