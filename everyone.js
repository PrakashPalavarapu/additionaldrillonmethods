// Write a function called `everyone` which returns an array of names of all the people in `got` variable.

const got = require("./test/gotData");
function everyone() {
  try {
    if (typeof got === "object") {
      peopleList = [];
      houses = got.houses;
      houses.map((house) => {
        Object.entries(house).map((houseData) => {
          if (houseData[0] == "people") {
            Object.entries(houseData[1]).map((peopleData) => {
              peopleList.push(peopleData[1]["name"]);
            });
          }
        });
      });
      return peopleList;
    } else {
      throw Error("given data in not an object");
    }
  } catch (error) {
    console.error(error.name, error.message);
  }
}

module.exports = everyone;
