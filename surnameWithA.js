// Write a function called `surnameWithA` which returns a array of names of all the people in `got` variable whoes surname is starting with `A`(capital a).

const got = require("./test/gotData");

function surnameWithA() {
  try {
    if (typeof got === "object") {
      houses = got.houses;
      return houses
        .reduce((houseData, house) => {
          houseData.push(
            house["people"].reduce((peopleData, people) => {
              if (people["name"].split(" ")[1][0] === "A") {
                peopleData.push(people["name"]);
              }
              return peopleData;
            }, [])
          );
          return houseData;
        }, [])
        .flat();
    } else {
      throw Error("given data is not an object");
    }
  } catch (error) {
    console.log(error.name, error.message);
  }
}

module.exports = surnameWithA;
