// Write a function called `peopleByHouses` which counts the total number of people in different houses in the `got` variable defined in `data.js` file.

const got = require("./test/gotData");

function peopleByHouses() {
  try {
    if (typeof got === "object") {
      houses = got.houses;
      let house_people = houses.reduce((peoplesAndHouses, house) => {
        peoplesAndHouses[house.name] = house["people"].length;
        return peoplesAndHouses;
      }, {});
      return Object.fromEntries(Object.entries(house_people).sort());
    } else {
      throw Error("given data in not an object");
    }
  } catch (error) {
    console.error(error.name, error.message);
  }
}
module.exports = peopleByHouses;
