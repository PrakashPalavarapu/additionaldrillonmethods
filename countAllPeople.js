//  Write a function called `countAllPeople` which counts the total number of people in `got` variable defined in `data.js` file.

const got = require("./test/gotData");

function countAllPeople() {
  try {
    if (typeof got == "object") {
      let houses = got.houses;
      return houses.reduce((countPeople, people) => {
        countPeople += people["people"].length;
        return countPeople;
      }, 0);
    } else {
      throw Error("given data is not an Object");
    }
  } catch (error) {
    console.error(error.name, error.message);
  }
}
module.exports = countAllPeople;
